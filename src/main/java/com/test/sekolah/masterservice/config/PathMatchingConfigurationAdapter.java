package com.test.sekolah.masterservice.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class PathMatchingConfigurationAdapter implements WebMvcConfigurer {
    private final AuthInterceptor authInterceptor;
    private final SecurityInterceptor securityInterceptor;

    @Autowired
    public PathMatchingConfigurationAdapter(AuthInterceptor authInterceptor, SecurityInterceptor securityInterceptor) {
        this.authInterceptor = authInterceptor;
        this.securityInterceptor = securityInterceptor;
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(authInterceptor);
        registry.addInterceptor(securityInterceptor);
    }
}
