package com.test.sekolah.masterservice.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.sekolah.masterservice.model.dto.response.ResMessageDto;
import com.test.sekolah.masterservice.model.dto.response.ResValidateTokenDto;
import com.test.sekolah.masterservice.service.AuthServiceRestTemplate;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.HandlerInterceptor;

import java.util.Collections;

@Configuration
public class AuthInterceptor implements HandlerInterceptor {

    private final AuthServiceRestTemplate authService;

    @Autowired
    public AuthInterceptor(AuthServiceRestTemplate authService) {
        this.authService = authService;
    }

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token = request.getHeader("authToken");
        if (token == null) {
            ResMessageDto message = new ResMessageDto(401, "You dont have permission to access this API", Collections.emptyList());
            response.setContentType("application/json");
            response.getWriter().write(convertObjectToJson(message));
            return false;
        }
        ResValidateTokenDto authDto = authService.getLogin(token);
        if (authDto == null){
            ResMessageDto message = new ResMessageDto(401, "You dont have permission to access this API", Collections.emptyList());
            response.setContentType("application/json");
            response.getWriter().write(convertObjectToJson(message));
            return false;
        }
        request.setAttribute("User", authDto);
        return true;
    }

    public String convertObjectToJson(ResMessageDto body) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(body);
    }
}

