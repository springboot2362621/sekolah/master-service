package com.test.sekolah.masterservice.config;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.test.sekolah.masterservice.model.dto.response.ResMessageDto;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import java.util.Collections;
import java.util.Objects;

@Component
public class SecurityInterceptor implements HandlerInterceptor {
    @Value("${header.security.apiKey}")
    private String apikey;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String key = request.getHeader("APIKey");

        if (!Objects.equals(key, apikey)) {
            ResMessageDto message = new ResMessageDto(401, "You dont have permission to access this API", Collections.emptyList());
            response.setContentType("application/json");
            response.getWriter().write(convertObjectToJson(message));
            return false;
        }
        return true;
    }

    public String convertObjectToJson(ResMessageDto body) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(body);
    }
}
