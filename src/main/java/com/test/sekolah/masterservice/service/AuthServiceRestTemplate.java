package com.test.sekolah.masterservice.service;

import com.test.sekolah.masterservice.model.dto.response.ResValidateTokenDto;

public interface AuthServiceRestTemplate {
    ResValidateTokenDto getLogin(String authToken);
}
