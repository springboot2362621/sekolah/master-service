package com.test.sekolah.masterservice.service;

import com.test.sekolah.masterservice.model.dto.request.ReqInsertKelasDto;
import com.test.sekolah.masterservice.model.dto.response.ResGetKelasDto;
import com.test.sekolah.masterservice.model.dto.response.ResMessageDto;

import java.util.List;

public interface KelasService {
    ResMessageDto<String> createKelas(ReqInsertKelasDto request);
    ResMessageDto<String> updateKelas(String uuidKelas,ReqInsertKelasDto request);
    ResMessageDto<List<ResGetKelasDto>> getListKelas(Integer page, Integer size, String search);
    ResMessageDto<ResGetKelasDto> getDetailKelas(String uuidKelas);
    ResMessageDto<String> deleteKelas(String uuidKelas);
}
