package com.test.sekolah.masterservice.service.impl;

import com.test.sekolah.masterservice.exception.DataNotFoundException;
import com.test.sekolah.masterservice.exception.DuplicateException;
import com.test.sekolah.masterservice.model.constant.ConstantVariable;
import com.test.sekolah.masterservice.model.constant.ExceptionMessage;
import com.test.sekolah.masterservice.model.dto.request.ReqInsertMataPelajaranDto;
import com.test.sekolah.masterservice.model.dto.response.ResGetMataPelajaran;
import com.test.sekolah.masterservice.model.dto.response.ResMessageDto;
import com.test.sekolah.masterservice.model.entity.MataPelajaranEntity;
import com.test.sekolah.masterservice.repository.MataPelajaranRepository;
import com.test.sekolah.masterservice.service.MataPelajaranService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class MataPelajaranServiceImpl implements MataPelajaranService {
    private final MataPelajaranRepository mataPelajaranRepository;

    public MataPelajaranServiceImpl(MataPelajaranRepository mataPelajaranRepository) {
        this.mataPelajaranRepository = mataPelajaranRepository;
    }

    @Override
    public ResMessageDto<String> createMataPelajaran(ReqInsertMataPelajaranDto request) {
        Optional<MataPelajaranEntity> mataPelajaranOpt = mataPelajaranRepository.findByNameContainingIgnoreCaseAndActiveIsTrue(request.getNamaMataPelajaran());
        if (mataPelajaranOpt.isPresent()){
            throw new DuplicateException(ExceptionMessage.MATA_PELAJARAN_DUPLICATE);
        }

        MataPelajaranEntity data = new MataPelajaranEntity();
        data.setName(request.getNamaMataPelajaran());
        mataPelajaranRepository.save(data);

        return new ResMessageDto<>();
    }

    @Override
    public ResMessageDto<String> updateMataPelajaran(String uuidMataPelajaran, ReqInsertMataPelajaranDto request) {
        Optional<MataPelajaranEntity> mataPelajaranOpt = mataPelajaranRepository.findById(UUID.fromString(uuidMataPelajaran));
        if (mataPelajaranOpt.isEmpty()){
            throw new DataNotFoundException(ExceptionMessage.MATA_PELAJARAN_NOT_FOUND + uuidMataPelajaran);
        }

        Optional<MataPelajaranEntity> nameMataPelajaranExiting = mataPelajaranRepository.findByNameContainingIgnoreCaseAndActiveIsTrue(request.getNamaMataPelajaran());
        if (nameMataPelajaranExiting.isPresent() && mataPelajaranOpt.get().getName() != nameMataPelajaranExiting.get().getName()){
            throw new DuplicateException(ExceptionMessage.MATA_PELAJARAN_DUPLICATE);
        }

        MataPelajaranEntity data = mataPelajaranOpt.get();
        data.setName(request.getNamaMataPelajaran());
        mataPelajaranRepository.save(data);

        return new ResMessageDto<>();
    }

    @Override
    public ResMessageDto<List<ResGetMataPelajaran>> getListMataPelajaran(Integer page, Integer size, String search) {
        int pageReq = (page != null && size != null) ? page - 1 : 0;
        int sizeReq = (page != null && size != null) ? size : Integer.MAX_VALUE;
        Pageable pageable = PageRequest.of(pageReq, sizeReq);

        List<ResGetMataPelajaran> result = new ArrayList<>();
        Page<MataPelajaranEntity> listMapel = mataPelajaranRepository.getListMataPelajaran(search,pageable);
        for (MataPelajaranEntity mapel : listMapel){
            ResGetMataPelajaran data = new ResGetMataPelajaran();
            data.setUuid(mapel.getUuid().toString());
            data.setNameMataPelajaran(mapel.getName());

            result.add(data);
        }
        return ResMessageDto.<List<ResGetMataPelajaran>>builder()
                .status(ConstantVariable.STATUS_SUCCESS)
                .message(ConstantVariable.SUCCESS)
                .data(result).build();
    }

    @Override
    public ResMessageDto<ResGetMataPelajaran> getDetailMataPelajaran(String uuidMataPelajaran) {
        Optional<MataPelajaranEntity> mataPelajaranOpt = mataPelajaranRepository.findById(UUID.fromString(uuidMataPelajaran));
        if (mataPelajaranOpt.isEmpty()){
            throw new DataNotFoundException(ExceptionMessage.MATA_PELAJARAN_NOT_FOUND + uuidMataPelajaran);
        }

        ResGetMataPelajaran result = new ResGetMataPelajaran();
        result.setUuid(mataPelajaranOpt.get().getUuid().toString());
        result.setNameMataPelajaran(mataPelajaranOpt.get().getName());

        return ResMessageDto.<ResGetMataPelajaran>builder()
                .status(ConstantVariable.STATUS_SUCCESS)
                .message(ConstantVariable.SUCCESS)
                .data(result).build();
    }

    @Override
    public ResMessageDto<String> deleteMataPelajaran(String uuidMataPelajaran) {
        Optional<MataPelajaranEntity> mataPelajaranOpt = mataPelajaranRepository.findById(UUID.fromString(uuidMataPelajaran));
        if (mataPelajaranOpt.isEmpty()){
            throw new DataNotFoundException(ExceptionMessage.MATA_PELAJARAN_NOT_FOUND + uuidMataPelajaran);
        }

        MataPelajaranEntity data = mataPelajaranOpt.get();
        data.setActive(false);
        mataPelajaranRepository.save(data);

        return new ResMessageDto<>();
    }
}
