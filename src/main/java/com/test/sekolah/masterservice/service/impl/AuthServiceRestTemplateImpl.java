package com.test.sekolah.masterservice.service.impl;

import com.test.sekolah.masterservice.model.dto.response.ResValidateTokenDto;
import com.test.sekolah.masterservice.service.AuthServiceRestTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;

@Component
public class AuthServiceRestTemplateImpl implements AuthServiceRestTemplate {
    private final RestTemplate restTemplate;
    @Value("${app.baseurl}")
    private String url;

    public AuthServiceRestTemplateImpl(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    @Override
    public ResValidateTokenDto getLogin(String authToken) {
        HttpHeaders headers = new HttpHeaders();
        headers.set("authToken", "Bearer " + authToken);
        headers.setBearerAuth(authToken);
        HttpEntity<String> entity = new HttpEntity<>(headers);
        try{
            ResponseEntity<ResValidateTokenDto> responseEntity = restTemplate.exchange(url+"auth/validate/token", HttpMethod.POST, entity, ResValidateTokenDto.class);
            ResValidateTokenDto authDto = new ResValidateTokenDto();
            if (responseEntity.getStatusCode().value() != 200){
                return authDto;
            }
            authDto = Objects.requireNonNull(responseEntity.getBody());
            return authDto;
        }catch (Exception e){
            return null;
        }
    }
}
