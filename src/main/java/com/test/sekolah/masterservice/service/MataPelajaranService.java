package com.test.sekolah.masterservice.service;

import com.test.sekolah.masterservice.model.dto.request.ReqInsertMataPelajaranDto;
import com.test.sekolah.masterservice.model.dto.response.ResGetMataPelajaran;
import com.test.sekolah.masterservice.model.dto.response.ResMessageDto;

import java.util.List;

public interface MataPelajaranService {
    ResMessageDto<String> createMataPelajaran(ReqInsertMataPelajaranDto request);
    ResMessageDto<String> updateMataPelajaran(String uuidMataPelajaran,ReqInsertMataPelajaranDto request);
    ResMessageDto<List<ResGetMataPelajaran>> getListMataPelajaran(Integer page, Integer size,String search);
    ResMessageDto<ResGetMataPelajaran> getDetailMataPelajaran(String uuidMataPelajaran);
    ResMessageDto<String> deleteMataPelajaran(String uuidMataPelajaran);
}
