package com.test.sekolah.masterservice.service.impl;

import com.test.sekolah.masterservice.exception.DataNotFoundException;
import com.test.sekolah.masterservice.exception.DuplicateException;
import com.test.sekolah.masterservice.model.constant.ConstantVariable;
import com.test.sekolah.masterservice.model.constant.ExceptionMessage;
import com.test.sekolah.masterservice.model.dto.request.ReqInsertKelasDto;
import com.test.sekolah.masterservice.model.dto.response.ResGetKelasDto;
import com.test.sekolah.masterservice.model.dto.response.ResMessageDto;
import com.test.sekolah.masterservice.model.entity.KelasEntity;
import com.test.sekolah.masterservice.repository.KelasRepository;
import com.test.sekolah.masterservice.service.KelasService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Service
public class KelasServiceImpl implements KelasService {

    private final KelasRepository kelasRepository;

    public KelasServiceImpl(KelasRepository kelasRepository) {
        this.kelasRepository = kelasRepository;
    }

    @Override
    public ResMessageDto<String> createKelas(ReqInsertKelasDto request) {
        Optional<KelasEntity> kelasOpt = kelasRepository.findByNameContainingIgnoreCaseAndActiveIsTrue(request.getNamaKelas());
        if (kelasOpt.isPresent()){
            throw new DuplicateException(ExceptionMessage.KELAS_DUPLICATE);
        }

        KelasEntity data = new KelasEntity();
        data.setName(request.getNamaKelas());
        kelasRepository.save(data);

        return new ResMessageDto<>();
    }

    @Override
    public ResMessageDto<String> updateKelas(String uuidKelas, ReqInsertKelasDto request) {
        Optional<KelasEntity> kelasOpt = kelasRepository.findById(UUID.fromString(uuidKelas));
        if (kelasOpt.isEmpty()){
            throw new DataNotFoundException(ExceptionMessage.KELAS_NOT_FOUND + uuidKelas);
        }

        Optional<KelasEntity> namaKelasExiting = kelasRepository.findByNameContainingIgnoreCaseAndActiveIsTrue(request.getNamaKelas());
        if (namaKelasExiting.isPresent() && kelasOpt.get().getName() != namaKelasExiting.get().getName()){
            throw new DuplicateException(ExceptionMessage.KELAS_DUPLICATE);
        }

        KelasEntity data = kelasOpt.get();
        data.setName(request.getNamaKelas());
        kelasRepository.save(data);

        return new ResMessageDto<>();
    }

    @Override
    public ResMessageDto<List<ResGetKelasDto>> getListKelas(Integer page, Integer size, String search) {
        int pageReq = (page != null && size != null) ? page - 1 : 0;
        int sizeReq = (page != null && size != null) ? size : Integer.MAX_VALUE;
        Pageable pageable = PageRequest.of(pageReq, sizeReq);

        List<ResGetKelasDto> result = new ArrayList<>();
        Page<KelasEntity> listKelas = kelasRepository.getListKelas(search,pageable);
        for (KelasEntity kelas : listKelas){
            ResGetKelasDto data = new ResGetKelasDto();
            data.setUuid(kelas.getUuid().toString());
            data.setName(kelas.getName());

            result.add(data);
        }
        return ResMessageDto.<List<ResGetKelasDto>>builder()
                .status(ConstantVariable.STATUS_SUCCESS)
                .message(ConstantVariable.SUCCESS)
                .data(result).build();
    }

    @Override
    public ResMessageDto<ResGetKelasDto> getDetailKelas(String uuidKelas) {
        Optional<KelasEntity> kelasOpt = kelasRepository.findById(UUID.fromString(uuidKelas));
        if (kelasOpt.isEmpty()){
            throw new DataNotFoundException(ExceptionMessage.KELAS_NOT_FOUND + uuidKelas);
        }

        ResGetKelasDto result = new ResGetKelasDto();
        result.setUuid(kelasOpt.get().getUuid().toString());
        result.setName(kelasOpt.get().getName());

        return ResMessageDto.<ResGetKelasDto>builder()
                .status(ConstantVariable.STATUS_SUCCESS)
                .message(ConstantVariable.SUCCESS)
                .data(result).build();
    }

    @Override
    public ResMessageDto<String> deleteKelas(String uuidKelas) {
        Optional<KelasEntity> kelasOpt = kelasRepository.findById(UUID.fromString(uuidKelas));
        if (kelasOpt.isEmpty()){
            throw new DataNotFoundException(ExceptionMessage.KELAS_NOT_FOUND + uuidKelas);
        }


        KelasEntity data = kelasOpt.get();
        data.setActive(false);
        kelasRepository.save(data);

        return new ResMessageDto<>();
    }
}
