package com.test.sekolah.masterservice.model.dto.request;

import lombok.Data;

@Data
public class ReqInsertMataPelajaranDto {
    private String namaMataPelajaran;
}
