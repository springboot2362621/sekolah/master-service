package com.test.sekolah.masterservice.model.constant;

public class ExceptionMessage {
    public static final String MATA_PELAJARAN_DUPLICATE = "Mata Pelajaran Sudah Ada";
    public static final String MATA_PELAJARAN_NOT_FOUND = "UUID mata pelajaran not found with uuid ";
    public static final String KELAS_DUPLICATE = "Kelas Sudah Ada";
    public static final String KELAS_NOT_FOUND = "UUID kelas not found with uuid ";
    public static final String INVALID_PERMISSION_API = "You dont have permission to access this API";
}
