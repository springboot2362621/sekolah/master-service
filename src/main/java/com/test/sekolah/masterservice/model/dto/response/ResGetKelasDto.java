package com.test.sekolah.masterservice.model.dto.response;

import lombok.Data;

@Data
public class ResGetKelasDto {
    private String uuid;
    private String name;
}
