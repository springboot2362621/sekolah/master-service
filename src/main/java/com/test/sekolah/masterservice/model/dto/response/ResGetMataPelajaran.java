package com.test.sekolah.masterservice.model.dto.response;

import lombok.Data;

@Data
public class ResGetMataPelajaran {
    private String uuid;
    private String nameMataPelajaran;
}
