package com.test.sekolah.masterservice.model.dto.request;

import lombok.Data;

@Data
public class ReqInsertKelasDto {
    private String namaKelas;
}
