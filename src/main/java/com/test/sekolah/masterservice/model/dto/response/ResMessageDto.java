package com.test.sekolah.masterservice.model.dto.response;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.test.sekolah.masterservice.model.constant.ConstantVariable;
import lombok.*;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ResMessageDto<T> {
    private int status=ConstantVariable.STATUS_SUCCESS;
    private String message= ConstantVariable.SUCCESS;

    @JsonInclude(JsonInclude.Include.NON_NULL)
    private T data;


}
