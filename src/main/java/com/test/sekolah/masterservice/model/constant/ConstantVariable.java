package com.test.sekolah.masterservice.model.constant;

public class ConstantVariable {
    public static final String USER = "User";
    public static final String SUCCESS = "Success";
    public static final Integer STATUS_SUCCESS = 200;
}
