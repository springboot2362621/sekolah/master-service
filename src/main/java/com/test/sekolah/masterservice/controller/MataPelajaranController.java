package com.test.sekolah.masterservice.controller;

import com.test.sekolah.masterservice.exception.AuthorizationException;
import com.test.sekolah.masterservice.model.constant.ConstantVariable;
import com.test.sekolah.masterservice.model.constant.ExceptionMessage;
import com.test.sekolah.masterservice.model.dto.request.ReqInsertMataPelajaranDto;
import com.test.sekolah.masterservice.model.dto.response.ResGetMataPelajaran;
import com.test.sekolah.masterservice.model.dto.response.ResMessageDto;
import com.test.sekolah.masterservice.model.dto.response.ResValidateTokenDto;
import com.test.sekolah.masterservice.service.MataPelajaranService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("/rest/v1/mapel")
public class MataPelajaranController {
    private final MataPelajaranService mataPelajaranService;

    public MataPelajaranController(MataPelajaranService mataPelajaranService) {
        this.mataPelajaranService = mataPelajaranService;
    }

    @PostMapping
    public ResponseEntity<ResMessageDto<String>> createMataPelajaran(@Valid @RequestBody ReqInsertMataPelajaranDto req,
                                                                     HttpServletRequest servletRequest) {
        ResValidateTokenDto authDto = (ResValidateTokenDto) servletRequest.getAttribute(ConstantVariable.USER);
        if (Boolean.FALSE.equals(authDto.getIsLogin())){
            throw new AuthorizationException(ExceptionMessage.INVALID_PERMISSION_API);
        }
        ResMessageDto<String> response = mataPelajaranService.createMataPelajaran(req);
        return ResponseEntity.ok().body(response);
    }

    @PutMapping
    public ResponseEntity<ResMessageDto<String>> updateUser(@RequestParam String uuidMataPelajaran,
                                                            @Valid @RequestBody ReqInsertMataPelajaranDto request,
                                                            HttpServletRequest servletRequest) {
        ResValidateTokenDto authDto = (ResValidateTokenDto) servletRequest.getAttribute(ConstantVariable.USER);
        if (Boolean.FALSE.equals(authDto.getIsLogin())){
            throw new AuthorizationException(ExceptionMessage.INVALID_PERMISSION_API);
        }
        ResMessageDto<String> response = mataPelajaranService.updateMataPelajaran(uuidMataPelajaran,request);
        return ResponseEntity.ok().body(response);
    }

    @GetMapping("/list")
    public ResponseEntity<ResMessageDto<List<ResGetMataPelajaran>>> getListUser(@RequestParam(required = false) Integer page,
                                                                                @RequestParam(required = false) Integer size,
                                                                                @RequestParam(required = false) String search,
                                                                                HttpServletRequest servletRequest) {
        ResValidateTokenDto authDto = (ResValidateTokenDto) servletRequest.getAttribute(ConstantVariable.USER);
        if (Boolean.FALSE.equals(authDto.getIsLogin())){
            throw new AuthorizationException(ExceptionMessage.INVALID_PERMISSION_API);
        }
        ResMessageDto<List<ResGetMataPelajaran>> response = mataPelajaranService.getListMataPelajaran(page, size, search);
        return ResponseEntity.ok().body(response);
    }

    @GetMapping("/detail")
    public ResponseEntity<ResMessageDto<ResGetMataPelajaran>> getDetailUser(@RequestParam String uuidMataPelajaran,
                                                                            HttpServletRequest servletRequest) {
        ResValidateTokenDto authDto = (ResValidateTokenDto) servletRequest.getAttribute(ConstantVariable.USER);
        if (Boolean.FALSE.equals(authDto.getIsLogin())){
            throw new AuthorizationException(ExceptionMessage.INVALID_PERMISSION_API);
        }
        ResMessageDto<ResGetMataPelajaran> response = mataPelajaranService.getDetailMataPelajaran(uuidMataPelajaran);
        return ResponseEntity.ok().body(response);
    }

    @DeleteMapping
    public ResponseEntity<ResMessageDto<String>> deleteUser(@RequestParam String uuidMataPelajaran,
                                                            HttpServletRequest servletRequest) {
        ResValidateTokenDto authDto = (ResValidateTokenDto) servletRequest.getAttribute(ConstantVariable.USER);
        if (Boolean.FALSE.equals(authDto.getIsLogin())){
            throw new AuthorizationException(ExceptionMessage.INVALID_PERMISSION_API);
        }

        ResMessageDto<String> response = mataPelajaranService.deleteMataPelajaran(uuidMataPelajaran);
        return ResponseEntity.ok().body(response);
    }
}
