package com.test.sekolah.masterservice.controller;

import com.test.sekolah.masterservice.exception.AuthorizationException;
import com.test.sekolah.masterservice.model.constant.ConstantVariable;
import com.test.sekolah.masterservice.model.constant.ExceptionMessage;
import com.test.sekolah.masterservice.model.dto.request.ReqInsertKelasDto;
import com.test.sekolah.masterservice.model.dto.response.ResGetKelasDto;
import com.test.sekolah.masterservice.model.dto.response.ResMessageDto;
import com.test.sekolah.masterservice.model.dto.response.ResValidateTokenDto;
import com.test.sekolah.masterservice.service.KelasService;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/rest/v1/kelas")
public class KelasController {
    private final KelasService kelasService;

    public KelasController(KelasService kelasService) {
        this.kelasService = kelasService;
    }


    @PostMapping
    public ResponseEntity<ResMessageDto<String>> createKelas(@Valid @RequestBody ReqInsertKelasDto req,
                                                                     HttpServletRequest servletRequest) {
        ResValidateTokenDto authDto = (ResValidateTokenDto) servletRequest.getAttribute(ConstantVariable.USER);
        if (Boolean.FALSE.equals(authDto.getIsLogin())){
            throw new AuthorizationException(ExceptionMessage.INVALID_PERMISSION_API);
        }
        ResMessageDto<String> response = kelasService.createKelas(req);
        return ResponseEntity.ok().body(response);
    }

    @PutMapping
    public ResponseEntity<ResMessageDto<String>> updateKelas(@RequestParam String uuidKelas,
                                                            @Valid @RequestBody ReqInsertKelasDto request,
                                                            HttpServletRequest servletRequest) {
        ResValidateTokenDto authDto = (ResValidateTokenDto) servletRequest.getAttribute(ConstantVariable.USER);
        if (Boolean.FALSE.equals(authDto.getIsLogin())){
            throw new AuthorizationException(ExceptionMessage.INVALID_PERMISSION_API);
        }
        ResMessageDto<String> response = kelasService.updateKelas(uuidKelas,request);
        return ResponseEntity.ok().body(response);
    }

    @GetMapping("/list")
    public ResponseEntity<ResMessageDto<List<ResGetKelasDto>>> getListKelas(@RequestParam(required = false) Integer page,
                                                                           @RequestParam(required = false) Integer size,
                                                                           @RequestParam(required = false) String search,
                                                                           HttpServletRequest servletRequest) {
        ResValidateTokenDto authDto = (ResValidateTokenDto) servletRequest.getAttribute(ConstantVariable.USER);
        if (Boolean.FALSE.equals(authDto.getIsLogin())){
            throw new AuthorizationException(ExceptionMessage.INVALID_PERMISSION_API);
        }
        ResMessageDto<List<ResGetKelasDto>> response = kelasService.getListKelas(page, size, search);
        return ResponseEntity.ok().body(response);
    }

    @GetMapping("/detail")
    public ResponseEntity<ResMessageDto<ResGetKelasDto>> getDetailKelas(@RequestParam String uuidKelas,
                                                                            HttpServletRequest servletRequest) {
        ResValidateTokenDto authDto = (ResValidateTokenDto) servletRequest.getAttribute(ConstantVariable.USER);
        if (Boolean.FALSE.equals(authDto.getIsLogin())){
            throw new AuthorizationException(ExceptionMessage.INVALID_PERMISSION_API);
        }
        ResMessageDto<ResGetKelasDto> response = kelasService.getDetailKelas(uuidKelas);
        return ResponseEntity.ok().body(response);
    }

    @DeleteMapping
    public ResponseEntity<ResMessageDto<String>> deleteKelas(@RequestParam String uuidKelas,
                                                            HttpServletRequest servletRequest) {
        ResValidateTokenDto authDto = (ResValidateTokenDto) servletRequest.getAttribute(ConstantVariable.USER);
        if (Boolean.FALSE.equals(authDto.getIsLogin())){
            throw new AuthorizationException(ExceptionMessage.INVALID_PERMISSION_API);
        }

        ResMessageDto<String> response = kelasService.deleteKelas(uuidKelas);
        return ResponseEntity.ok().body(response);
    }
}
