package com.test.sekolah.masterservice.repository;

import com.test.sekolah.masterservice.model.entity.MataPelajaranEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import javax.swing.text.html.Option;
import java.util.Optional;
import java.util.UUID;

public interface MataPelajaranRepository extends JpaRepository<MataPelajaranEntity, UUID> {
    Optional<MataPelajaranEntity> findByNameContainingIgnoreCaseAndActiveIsTrue(String name);

    @Query("SELECT MPE " +
            "FROM MataPelajaranEntity MPE " +
            "WHERE (:search is null OR :search = '' OR " +
            "LOWER(MPE.name) LIKE LOWER(CONCAT('%',:search,'%'))) " +
            "AND MPE.active = TRUE ")
    Page<MataPelajaranEntity> getListMataPelajaran(@Param("search") String search,
                                                   Pageable pageable);
}
