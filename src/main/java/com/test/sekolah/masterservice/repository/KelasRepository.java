package com.test.sekolah.masterservice.repository;

import com.test.sekolah.masterservice.model.entity.KelasEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.Optional;
import java.util.UUID;

public interface KelasRepository extends JpaRepository<KelasEntity, UUID> {
    Optional<KelasEntity> findByNameContainingIgnoreCaseAndActiveIsTrue(String name);

    @Query("SELECT KE " +
            "FROM KelasEntity KE " +
            "WHERE (:search is null OR :search = '' OR " +
            "LOWER(KE.name) LIKE LOWER(CONCAT('%',:search,'%'))) " +
            "AND KE.active = TRUE ")
    Page<KelasEntity> getListKelas(@Param("search") String search,
                                                   Pageable pageable);
}
