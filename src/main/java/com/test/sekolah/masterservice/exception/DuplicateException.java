package com.test.sekolah.masterservice.exception;

import java.io.Serial;

public class DuplicateException extends RuntimeException{
    @Serial
    private static final long serialVersionUID = -2699688370557886351L;
    public DuplicateException(String message) {
        super(message);
    }
}
