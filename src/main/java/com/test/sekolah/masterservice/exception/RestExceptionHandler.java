package com.test.sekolah.masterservice.exception;

import com.test.sekolah.masterservice.model.dto.response.ResMessageDto;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class RestExceptionHandler {

    @ExceptionHandler({DuplicateException.class, RequiredFieldIsMissingException.class,
            RequiredFieldNotValidException.class,IOException.class,
            BadRequestException.class, GeneralSecurityException.class, IOException.class,
            HttpMessageNotReadableException.class, IllegalArgumentException.class,
            DataNotFoundException.class,AuthorizationException.class})
    public ResponseEntity<ResMessageDto> userNotFoundException(RuntimeException exception) {
        ResMessageDto message = new ResMessageDto(HttpStatus.BAD_REQUEST.value(), exception.getMessage(), null);
        return new ResponseEntity<>(message, HttpStatus.BAD_REQUEST);
    }


    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ResMessageDto> handleArgumentNotValidException(MethodArgumentNotValidException exception) {
        List<String> errors = new ArrayList<>();
        exception.getBindingResult().getFieldErrors().forEach(error -> errors.add(error.getDefaultMessage()));
        ResMessageDto message = new ResMessageDto(HttpStatus.INTERNAL_SERVER_ERROR.value(), "Invalid required field constraints.", errors);
        return new ResponseEntity<>(message, HttpStatus.INTERNAL_SERVER_ERROR);
    }
}
