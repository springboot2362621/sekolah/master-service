package com.test.sekolah.masterservice.exception;

import java.io.Serial;

public class RequiredFieldNotValidException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = 3834385102327726374L;

    public RequiredFieldNotValidException(String message) {
        super(message);
    }
}
