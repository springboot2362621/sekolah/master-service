package com.test.sekolah.masterservice.exception;

import java.io.Serial;

public class RequiredFieldIsMissingException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = 4123371684538937348L;

    public RequiredFieldIsMissingException(String message) {
        super(message);
    }

}
