package com.test.sekolah.masterservice.exception;

import java.io.Serial;

public class DataNotFoundException extends RuntimeException {

    @Serial
    private static final long serialVersionUID = 6829813883332232907L;
    public DataNotFoundException(String message) {
        super(message);
    }
}
